"""PyIDE completion plugin."""

from jedi.api import Script
from gi.repository import GObject, Gtk, GtkSource, GdkPixbuf


class Jedi:
    """Docstring for Jedi."""

    def get_script(document):
        """Docstring for det_script."""
        doc_text = document.get_text(document.get_start_iter(),
                                     document.get_end_iter(),
                                     False)
        iter_cursor = document.get_iter_at_mark(document.get_insert())
        linenum = iter_cursor.get_line() + 1
        charnum = iter_cursor.get_line_index()

        return Script(doc_text, linenum, charnum, 'py')


class IdeCompletionProvider(GObject.GObject, GtkSource.CompletionProvider):
    """Docstring for IdeCompletionProvider."""

    __gtype_name__ = 'IdeProvider'

    # FIXME: find real icon names
    icon_names = {
        'import': 'object-straighten-symbolic',
        'module': 'object-straighten-symbolic',
        'class': 'insert-object-symbolic',
        'function': 'func',
        'statement': 'insert-link-symbolic',
        'param': ''
    }

    def __init__(self):
        """Docstring deafult."""
        GObject.GObject.__init__(self)

    def do_get_name(self):
        """Docstring deafult."""
        # TODO create a better name
        return "Ide completion"

    def do_match(self, context):
        """Docstring deafult."""
        # FIXME check if the text is in betweem quotes or in a comment
        start_iter, end_iter = context.get_iter()

        end_iter.backward_char(1)
        _char = end_iter.get_char()

        if not (_char in ('_', '.') or _char.isalnum()):
            return False

        return True

    def do_get_priority(self):
        """Docstring deafult."""
        return 1

    def do_get_activation(self):
        """Docstring deafult."""
        return GtkSource.CompletionActivation.INTERACTIVE

    def do_populate(self, context):
        """Docstring deafult."""
        start_iter, end_iter = context.get_iter()
        buffer = end_iter.get_buffer()
        proposals = []

        for completion in Jedi.get_script(buffer).completions():

            print(completion)

            complete = completion.name

            item = GtkSource.CompletionItem.new2()
            item.set_icon(self._get_icon_for_type(completion.type))
            item.set_label(complete)
            item.set_text(completion.type)  # Trying out
            item.set_info(completion.docstring())

            proposals.append(item)

    def _get_icon_for_type(self, _type):

        theme = Gtk.IconTheme.get_default()

        # If the completion type is unknown return default icon
        if not _type.lower() in self.icon_names:
            return theme.load_icon(Gtk.STOCK_ADD, 16, 0)

        try:

            if 'symbolic' in self.icon_names[_type.lower()]:
                return theme.load_icon(self.icon_names[_type.lower()], 16, 0)
            else:
                return GdkPixbuf.Pixbuf.new_from_file_at_scale(
                        # icon name must be it's path
                        self.icon_names[_type.lower()],
                        16, 16, True)

        except Exception:
            # Just in case something really bad happens
            try:
                return theme.load_icon(Gtk.STOCK_ADD, 16, 0)
            except Exception:
                return None


class Plugin(GObject.GObject):
    """The plugin class."""

    def __init__(self, *args, **kwargs):
        """Docstring deafult."""
        GObject.GObject.__init__(self)

        self.attaches = {}

        if 'application_window' in kwargs:
            self.application_window = kwargs['application_window']

    def do_activate(self):
        """Docstring deafult."""
        if self.application_window is not None:
            self.application_window.editor.connect("editor-created-after", self.do_attach_to_editor)

    def do_attach_to_editor(self, *args):
        """Docstring deafult."""
        if self.application_window.current_editor is not None:
            """ Call completion connection """
            # [...] code

            # If the current editor is already connected
            if self.application_window.current_editor in self.attaches:
                return


# Registering the type on GObject
GObject.type_register(IdeCompletionProvider)
